cable_routing_07.pdf: how_many_turbines_06.tex
	pdflatex how_many_turbines_06.tex
	bibtex how_many_turbines_06.aux

lastdiff.pdf: how_many_turbines_06.tex
	git cat-file blob HEAD:how_many_turbines_06.tex > /tmp/HEAD.tex
	latexdiff --config PICTUREENV=CD /tmp/HEAD.tex how_many_turbines_06.tex > lastdiff.tex
	pdflatex lastdiff
	bibtex lastdiff
	pdflatex lastdiff
	pdflatex lastdiff
