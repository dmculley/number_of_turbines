\documentclass[authoryear]{elsarticle}
%% \usepackage{tgpagella}
\usepackage[T1]{fontenc}
\usepackage{dsfont}
\usepackage{graphicx}
%\usepackage{harvard}
%\usepackage{cite}
\usepackage{subfigure}
\usepackage{pdflscape}
\usepackage{lscape}
\usepackage{float}
\usepackage{natbib} % need this
\usepackage{amssymb,amsmath}
%\usepackage{algpseudocode}
%\usepackage[top=2cm,bottom=2cm,left=2.2cm,right=2.2cm]{geometry}
\usepackage{setspace}
\usepackage{indentfirst}
\usepackage[font=small,format=plain,labelfont=bf,up,%% textfont=it
  ,up]{caption}
\usepackage{tikz}

\usetikzlibrary{calc,3d, shapes,arrows, positioning}
\setlength{\parindent}{10pt}
\setlength{\parskip}{7pt}

\renewcommand{\vec}[1]{\mathbf{#1}}
%\renewcommand{\vec}[1]{{\mbox{\boldmath$#1$}}}

%% \doublespacing    
\newtheorem{theorem}{Theorem}

\bibliographystyle{plainnat} % need this

\title{Abstract}
\author{}
\date{}

\begin{document}

\section*{Gaussian Processes}

A Gaussian process is formally defined as a collection of random variables, any finite number of which have a joint Gaussian distribution \citep{rasmussen2006gaussian}. While a Gaussian process represents a distribution over a \textit{function}, a Gaussian distribution represents the distribution over a finite number of points \citep{murphy2012machine}. In application, the ability to define the distribution over arbitrary points is all that is required, as the function can be represented more or less accurately by varying the number of evaluation points and their spacing.

A Gaussian process represents a `prior' distribution over functions through choice of a mean and kernel function. These choices encode prior assumptions about the behaviour of the function being sought -- for example that the similarity of two outputs will be related to the similarity of their inputs. `Training' data is then introduced which produces a `posterior' distribution over functions. The posterior represents a probability distribution of the function evaluated at the chosen unknown points.

A Gaussian process, $\mathcal{GP}$, of a function, $f$ acting upon a collection of independent variables, $\vec{x}$, may be fully described by a mean function, $m(\vec{x}) = \mathbb{E}[f(\vec{x})]$ and a kernel function $\kappa(\vec{x}, \vec{x}') = \mathbb{E}[(f(\vec{x}) - m(\vec{x}))(f(\vec{x}') - m(\vec{x}'))]$ as 
\begin{equation}
f(\vec{x}) \sim \mathcal{GP}(m(\vec{x}), \kappa(\vec{x}, \vec{x}')).
\end{equation}

Given a finite number, $n \in \mathbb{N}$, of inputs, $\vec{X}$, we can define a Gaussian distribution, $\mathcal{N}$, on that set by evaluating the mean function on those inputs, $\boldsymbol{\mu} = m(\vec{X})$ and evaluating the kernel function between those points to produce a matrix, $K = K(\vec{X}, \vec{X})$ where $K_{ij} = k(\vec{x}_i, \vec{x}_j) ~\forall~ i, j \in n$;
\begin{equation} \label{eq:N}
\vec{f} \sim \mathcal{N}(\boldsymbol{\mu}, \vec{K}).
\end{equation}

Clearly the choice of mean and kernel functions have a strong influence upon the accuracy of posterior based predictions. It is this utility to choose and optimise these functions that makes Gaussian processes such a powerful and flexible tool. To begin with, the common assignments of $m(\vec{x}) = 0$ and 
\begin{equation} \label{eq:SE}
\kappa (\vec{x},\vec{x}') =  \sigma ^2_f \exp \left(- \frac{(\vec{x} - \vec{x}')^2}{2l^2} \right),
\end{equation}
will be used. \eqref{eq:SE} is a very common kernel function which returns the squared exponential difference between two inputs. The \textit{hyper-parameters}, $\sigma ^2_f$ and $l$, enable the fine tuning of the expression based upon the behaviour that is expected and our confidence in those expectations. These can be automatically tuned based upon a maximisation of the expectation of returning observed points, a process which shall be discussed in detail further on.

Having chosen mean and kernel functions, a prior can be constructed. Given a number, $n_*$, of unknown inputs (so called `test' values), $\vec{X}_*$ this can be used to determine the probability distribution of $f(\vec{x}_i) ~\forall~ \vec{x}_i \in \vec{X}_*$. Clearly this will be a Gaussian distribution centered on $f = 0$ (as shown in figure \ref{fig:GPR}) -- which is not hugely informative in itself. However, by including known data within the framework the uncertainty can be reduced and a useful model constructed, this is the posterior distribution. The known data is set of, $n$, observations called the `training set', $\mathcal{D}$ and has inputs arranged in the matrix $\vec{X}_\mathcal{D}$. In the general form, $\mathcal{D} = \{(\vec{x}_i, f_i) ~|~ i = 1: n \}$. These fit into \eqref{eq:N} to produce
\begin{equation}  \label{eq:ND}
\vec{f}_\mathcal{D} \sim \mathcal{N}(\boldsymbol{\mu}_\mathcal{D}, \vec{K}_{\mathcal{D}\mathcal{D}}).
\end{equation}
Since the test values belong to the same distribution, they can be appended to \eqref{eq:ND} to obtain the joint distribution
\begin{equation} \label{eq:joint}
\binom{\vec{f}_\mathcal{D}}{\vec{f}_*} \sim \mathcal{N} \left( \binom{\boldsymbol{\mu}_\mathcal{D}}{\boldsymbol{\mu}_*} , \begin{pmatrix} \vec{K}_{\mathcal{D}\mathcal{D}} & \vec{K}_{\mathcal{D}*} \\ \vec{K}^T_{\mathcal{D}*} & \vec{K}_{**} \end{pmatrix} \right),
\end{equation}
where, $\vec{K} = \vec{K}(\vec{X}, \vec{X})$ is $n \times n$, $\vec{K_*} = \vec{K}(\vec{X}, \vec{X_*})$ is $n \times n_*$ and $\vec{K}_{**} = \vec{K}(\vec{X_*}, \vec{X_*})$ is $n_* \times n_*$.

From \eqref{eq:joint} it is possible to calculate the probability distribution of the output given an input and our training set via the following theorem:

\begin{theorem}
Suppose $\vec{x} = (\vec{x}_1, \vec{x}_2)$ is jointly Gaussian with parameters

\begin{equation}
\boldsymbol{\mu} = \binom{\boldsymbol{\mu}_1}{\boldsymbol{\mu}_2}, ~~ \boldsymbol{\Sigma} = \begin{pmatrix} \boldsymbol{\Sigma}_{11} & \boldsymbol{\Sigma}_{12} \\\boldsymbol{\Sigma}^T_{12} & \boldsymbol{\Sigma}_{22} \end{pmatrix}
\end{equation}
i.e.
\begin{equation}
\vec{x} \sim \mathcal{N} \left( \boldsymbol{\mu}, \boldsymbol{\Sigma} \right)
\end{equation}
and
\begin{equation}
\begin{aligned}
p(\vec{x}_1) = \mathcal{N} \left( \vec{x}_1 | \boldsymbol{\mu}_1, \boldsymbol{\Sigma}_{11} \right) \\
p(\vec{x}_2) = \mathcal{N} \left( \vec{x}_2 | \boldsymbol{\mu}_2, \boldsymbol{\Sigma}_{22} \right)
\end{aligned}
\end{equation}
then the posterior conditional is given by
\begin{equation} \label{eq:MVN}
\begin{aligned}
p(\vec{x}_1 | \vec{x}_2) &= \mathcal{N}(\vec{x}_1 | \boldsymbol{\mu}_{1|2} , \boldsymbol{\Sigma}_{1|2}) \\
\boldsymbol{\mu}_{1|2} &= \boldsymbol{\mu}_1 + \boldsymbol{\Sigma}_{1|2} \boldsymbol{\Sigma}_{22}^{-1} (\vec{x}_2 - \boldsymbol{\mu}_2) \\
\boldsymbol{\Sigma}_{1|2} &= \boldsymbol{\Sigma}_{11} - \boldsymbol{\Sigma}_{12} \boldsymbol{\Sigma}_{22}^{-1} \boldsymbol{\Sigma}_{12}^T
\end{aligned}
\end{equation}
\end{theorem}

Applying theorem 1 gives the posterior,
\begin{equation} \label{eq:MVN_applied}
\begin{aligned}
p(\vec{f}_* | \vec{f}_\mathcal{D}) &= \mathcal{N}(\vec{f}_* | \boldsymbol{\mu}_{*|\mathcal{D}} , \vec{K}_{*|\mathcal{D}}) \\
\boldsymbol{\mu}_{*|\mathcal{D}} &= \boldsymbol{\mu}_* + \vec{K}_{*|\mathcal{D}} \vec{K}_{\mathcal{D}\mathcal{D}}^{-1} (\vec{f}_\mathcal{D} - \boldsymbol{\mu}_\mathcal{D}) \\
\vec{K}_{*|\mathcal{D}} &= \vec{K}_{**} - \vec{K}_{*\mathcal{D}} \vec{K}_{\mathcal{D}\mathcal{D}}^{-1} \vec{K}_{*\mathcal{D}}^T.
\end{aligned}
\end{equation}
This is shown in figure \ref{fig:GPR}.

It cannot be overstated that the performance of the Gaussian process model (in terms of the accuracy of the predictions) is entirely dependent upon a well chosen and parametrised mean and kernel function. Fortunately, for this application, it is possible (though time consuming) to produce large sets of training data for varied circumstances (different meshes, turbine types, bottom frictions etc) and thus select a kernel which best suits the task of predicting the performance of arrays of different numbers of turbines. More fine scale tuning can then be conducted upon a case by case basis through the adjustment of the hyper-parameters within the mean and kernel function. This adjustment can be automated through a gradient descent optimisation to select the parameters which maximise the marginal liklihood. 

\begin{figure}[h!]
\centering
\includegraphics[width = 1\textwidth, keepaspectratio=true]{plots/GPR.JPG}
\caption{Prior distribution (top) with 4 samples shown. Bottom panel, shows posterior based upon the marked training set.}
\label{fig:GPR}
\end{figure}



\section*{Bibliography}
\bibliography{/home/dmc13/Dropbox/dphil/central_bib/bibio.bib}

\end{document}
