\documentclass[authoryear]{elsarticle}

\usepackage{natbib}
\usepackage{subfig}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{color}
\usepackage{url}
\usepackage{booktabs}
\usepackage{amsthm}

\usepackage{tikz}
\usetikzlibrary{calc,3d, shapes,arrows, positioning}


\newsavebox{\tempbox}

\clubpenalty = 10000 % schliesst Schusterjungen aus 
\widowpenalty = 10000 \displaywidowpenalty = 10000% schliesst Hurenkinder aus

\journal{ -- INSERT HERE -- }

\newcommand{\diff}{\,\mathrm{d}}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\dx}{\,\mathrm{d}x}

%% \doublespacing    

%\bibliographystyle{plainnat} % need this

\title{A surrogate-assisted approach for \\ optimising the size of tidal turbine arrays}
	
\author{D.M. Culley$^{~a*}$, S.W. Funke$^{~a,b}$, S.C. Kramer$~^a$ \& M.D. Piggott$~^{a,c}$}
	
\address{$^a$ Applied Modelling and Computation Group, Department of \\ Earth Science and Engineering, Imperial College London, London, UK}
	
\address{$^b$ Center for Biomedical Computing, Simula Research Laboratory, Oslo, Norway}

\address{$^c$ Grantham Institute for Climate Change, Imperial College London, London, UK}

\address{$^*$ Corresponding author. dmc13@imperial.ac.uk}

\begin{document}

\begin{frontmatter}

\begin{abstract}

Scenario specific factors, such as site characteristics, technological constraints and practical engineering considerations greatly impact upon both the appropriate number of turbines to include within a tidal current turbine array (array size), and the individual locations of those turbines (turbine micro-siting). Both have been shown to significantly impact upon the energy yield of an array. Consequently, evaluating the power output of a given number of turbines requires an optimisation of the turbine micro-siting of those turbines on the proposed site. As such, in the course of optimising the \textit{array size}, a power evaluation at each iteration requires a micro-siting optimisation. In this work, this `inner' optimisation is treated as a computationally expensive solver, mapping array size to power, and a practical approach to the array size optimisation problem is introduced in which a surrogate model is used to maximise the the utility of each evaluation made by the solver.

In general, beyond a certain number, as additional turbines are introduced into the flow, the power output of the array \textit{per turbine} is reduced. While the power output of the array may be maximised by installing a large array populated by many turbines, the developer will see a diminishing return on the investment in additional turbines beyond an optimal number. Having demonstrated the benefits of a surrogate-assisted approach to array optimisation, a financial model is implemented to explore how the array design process is affected by the incorporation of practical considerations, such as maximisation of return on investment.

\begin{keyword}
marine renewable energy \sep tidal turbines \sep gradient-based optimisation \sep adjoint method \sep shallow water equations \sep array layout \sep cable-routing
\end{keyword}

\end{abstract}

\end{frontmatter}

\section{Introduction}

%In recent years, marine energy has become an emergent sector in the renewable energy industry, with interest driven largely by the significant contribution tidal stream energy could potentially make as a part of the UK's renewable energy portfolio \citep{Johnstone2013}. While methods for evaluating the exploitable tidal resource vary, it is generally considered that the national tidal power asset could amount to around a fifth of the UK's electricity consumption \citep{Trust2011} and 10 to 15\% of the harvestable global tidal power resource \citep{Johnstone2013}. That notwithstanding, the global market is also developing, with notable potential tidal power assets in Canada, Argentina, France, Ireland, Russia, Australia and China \citep{ORourke2010}. The rate of research into tidal stream energy has increased in recent years, led by academia and fueled by private industry benefiting from offshore expertise from a North Sea oil and gas industry in decline \citep{Charlier2003}. As such, the potential economic impact of the sector is significant, both through facilitating energy autonomy by increasing domestic generation and through development of an industry with exportable expertise.

Tidal current turbines (TCTs) use the momentum of tidal currents to generate electricity. Characteristics such as the high power density of the resource and the reliability with which it can be forecast make tidal current power conversion an attractive technology for development. It has been demonstrated by \citet{Funke2013} that the energy yield of an array of a given number of turbines can be heavily contingent upon the the micro-siting design of the array; that is, the spatial arrangement and placement of the individual turbines. This is because there is a high spatial variation in flow speed caused by the turbines' wakes coupled with which, the power extracted by a turbine has a cubic dependency upon that flow speed.

\citet{BrydenCouch2006} demonstrated that harvesting energy from a flow has a sufficiently significant effect upon that flow that it is imperative that the tidal array designer design for the \textit{resultant} flow. That is to say, when modelling an array, the flow must be calculated with the turbines in place. Consequently, in the process of an optimisation, each iteration in which the micro-siting design is adjusted requires the flow to be recalculated. The more detail in which the physical processes are modelled, the more confidence may be placed in the solution but, of course, the more computationally expensive the solve. 

Assuming that during the micro-siting optimisation, each turbine is free to move in plane, there is a $2n$ dimensional space in which the optimal turbine arrangement resides. Thus micro-siting design optimisation is challenge of efficiently targeting the available computational resource to search a large solution space of configurations while evaluating each configuration with an accurate flow solve.

While the micro-siting problem has received some limited attention, the more fundamental question of how many turbines there should be in the first place, has been given very little consideration. Accurately forecasting the potential power output of an array of $n$ turbines requires a micro-siting optimisation. Therefore significant computational effort is expended each time a different $n$ is sampled, so building up a picture of the relationship between $n$ and the power output for a given site is not straightforward.

The novel contribution of this study is the proposal of a practical methodology to study this relationship. The goal is that this approach may be adopted to scope projects on potentially complex sites and to specify the optimal design of potentially large arrays, both in terms of turbine numbers and their positions. In this work, use of developer's return upon investment is proposed as the metric of array design quality, and a simple financial model is proposed to demonstrate the utility of this approach.

In the following section, the problem is formulated mathematically and split into an `inner' continuous and `outer' integer optimisation problem. The `inner' micro-siting design optimisation approach is detailed in section \ref{sec:MS} and the implementation of a surrogate model for the `outer' optimisation is explored in section \ref{sec:AS}. The ensemble model is detailed in section \ref{sec:ES} and applied to a simple, idealised, test case in section \ref{sec:EX1} to demonstrate the limitations of optimising array design to solely maximise power extraction. As a solution, a financial perspective is proposed in section \ref{sec:CM}, in which the array design may be optimised for the developer's return upon investment, and to this end, a simple financial model is implemented. Finally, the financial optimisation approach is demonstrated, first on the initial idealised example, then on a more realistic case based on the Inner Sound of the Pentland Firth.


\section{Problem Formulation} \label{sec:PF}

Our initial goal is to determine the optimum number of turbines, $N$, and their locations to maximise the power output, $P$, of a tidal current turbine array. If we encode the Cartesian coordinates for each turbine in a vector, $\vec{m}$ where $\vec{m} = ((x_i, y_i) ~|~ i = 1, \dots, N)^T$ the problem may be expressed as

\begin{equation} \label{eq:power_overall}
\underset{N, \vec{m}}{\text{max}} \hspace{0.5cm} P(\vec{m}|_N),
\end{equation} %\hfill <insert citation>

\noindent where the $|_N$ notation indicates the length of the preceding vector. This can be separated into an integer optimisation problem controlling the \textit{number} of turbines in the array; 

\begin{equation} \label{eq:int_main}
\underset{N}{\text{max}} \hspace{0.5cm} \tilde{P}(N),
\end{equation} %\hfill

\noindent where $\tilde{P}(N)$ is in turn an optimisation problem returning the power for the optimal location of $N$ turbines;

\begin{equation} \label{eq:cont_main}
\begin{aligned}
& \tilde{P}(N) = \underset{\vec{m}}{\text{max}}
& & \text{Power}(\vec{m})  \\
& \text{~~~~~~~~~~~~subject to}
& & b_l \leq \vec{m} \leq b_u \\ 
&
& & g(\vec{m}) \leq 0.
\end{aligned}
\end{equation} \hfill ~\citep{Funke2013}

\noindent here, the constraint $b_l \leq \vec{m} \leq b_u$ limits the turbines to within the boundaries of the turbine area that has been leased for development, and a minimum allowable distance constraint between turbines is represented by the inequality constraint $g(\vec{m}) \leq 0$.

The power of the array is computed as sum of the power extracted from the flow due to the presence of the turbines. Turbines are modelled as areas of increased friction, described by $c_t$,

\begin{equation}
\text{Power}(\vec{m}) = \int_\Omega \rho c_t(\vec{m})||\vec{u}||^3 \dx.
\end{equation}

\noindent Here, $\rho$ is the density of the water and $\vec{u}$ is the depth-averaged flow velocity which is the solution to the shallow water equations

\begin{equation} \label{eq:SW}
\begin{aligned}
\vec{u} \cdot \nabla \vec{u} - \nu \nabla ^2 \vec{u} + g \nabla \vec{\eta} + \frac{c_b+c_t(\vec{m})}{H} ||\vec{u}|| \vec{u} = 0,\\
\nabla \cdot(H\vec{u}) = 0.
\end{aligned}
\end{equation}

\noindent These are discretised using the Taylor-Hood element pairs \citep{taylor1973numerical} and solved using the finite element method over a triangular mesh. The viscosity coefficient, acceleration due to gravity, resting water depth and quadratic bottom friction respectively are represented by $\nu$, $g$, $H$ and $c_b$ respectively.


\section{Optimisation of micro-siting using the adjoint} \label{sec:MS}

An approach to determining the optimal locations of turbines within an array is packaged in the code \textit{OpenTidalFarm} \citep{Funke2013}. \textit{OpenTidalFarm} formulates the turbine layout problem in the form of \eqref{eq:cont_main}; as an optimisation problem constrained by the shallow water equations. It uses an efficient gradient based method to maximise the power output of the farm by controlling the locations of the individual turbines. 

The model is initialised with $N$ turbines placed in an initially guessed layout within the turbine area. The steady-state shallow water equations \eqref{eq:SW} are solved and the power extracted from the flow by the array is calculated. This process maps $\vec{m} \in \mathbb{R}^{2N} \to P \in \mathbb{R}$. The gradient of the power with respect to the turbine positions, $\diff P / \diff \vec{m}$ is then computed by solving the associated adjoint equations. The power and the gradient are then used to improve the layout of the turbines for the next iteration.

The adjoint approach is particularly suited to determining the gradient of a functional (a function mapping to a scalar) as it can do so at a computational cost independent of the number of input parameters (in this case the $x$ and $y$ coordinates of each turbine). Computationally cheap access to gradient data means that gradient based optimisation methods may be used to improve the turbine layout. Such gradient-based optimisation enables convergence in orders of magnitude fewer iterations than many alternate optimisation approaches, such as genetic algorithms. The benefit of requiring fewer iterations is that less computational economy is required for each iteration, and so a more detailed -- and therefore more realistic -- flow model may be used.


\section{Optimisation of array size} \label{sec:AS}

The optimisation of $\vec{m}$ by \textit{OpenTidalFarm} can be thought of as an unknown, non-linear solver mapping the size of an array to the maximum power output attainable from those turbines; $N \in \mathbb{R} \to P \in \mathbb{R}$. Having established that a calling this solver to evaluate $P(N)$ is computationally expensive, the goal is to minimise the number of times we have to call it to determine the optimum $N$. This is achieve by introducing a computationally cheap model based on a small number of data points for the `outer', array size, optimisation, from which the function at unknown points may be estimated. Such a model is called a surrogate.

A surrogate is a model which simulates the \textit{outcome} of the `full' model by disregarding the mechanics of the full model, and instead creating a map from input to output based upon prior observations -- i.e. empirical data \citep{jin2011surrogate}. Of course, it is worth noting that all computational models are abstractions to real-world processes and a surrogate model may simply be thought of as an additional abstraction: In the case of tidal arrays, full scale ocean testing is prohibitively expensive, as -- most likely -- is testing in a flume, moving down in orders of realism, high resolution 3D numerical models on the array scale are computationally very demanding and so complexity and therefore realism continues to be reduced until the expense is manageable and the abstracted problem is tractable. 

\textit{OpenTidalFarm} is designed to strike a balance between acceptable run-time and sufficiently realistic assumptions. By adjusting the coarseness of the mesh and controlling the convergence criteria, the computational expense of an \textit{OpenTidalFarm} solve can be managed. Of course, a reduction in the computational effort comes at the cost of some of the model's ability to capture the real-world mechanics of the problem and thus there is a trade off between computational cost and accuracy of the solution.

 \citet{lim2010generalizing} term the benefits of simplification as the `\textit{bless of uncertainty}' and the negative consequences as the `\textit{curse of uncertainty}'. A well chosen and effectively employed surrogate can be used to control \textit{OpenTidalFarm} solver parameters (such as mesh size and convergence criteria) during the course of the integer optimisation, in order to balance these beneficial and detrimental effects of simplification.

The requirement for this work therefore is for a model which chooses a small number of sensible values of $N$ at which to evaluate $P(N)$ and to use these to construct an estimation of the function at the un-tested values. Ideally the surrogate should be able to quantify the uncertainty of this estimation and use this to actively control the parameters in \textit{OpenTidalFarm} which balance computational cost and accuracy. 

To achieve this a home-grown collocation selection algorithm coupled with a standard Gaussian process regression, GPR, approach. the collocation algorithm determines the sample points and controls the settings for \textit{OpenTidalFarm}, while GPR is a non-parametric regression model for creating an input-output mapping from a training set of empirical data \citep{rasmussen2006gaussian}.


\subsection{Gaussian Process Regression}

Gaussian processes are useful in the general case where we have input features, $x_1, x_2, ..., x_m$ arranged in a vector $\vec{x}$ and wish to study the response of a function, $f(\vec{x})$, to variation in those features. The power of GPR is that it does not require the definition of a mathematical object describing $f(\vec{x})$. All that is required is the ability to evaluate the function at an arbitrary point $\vec{x}_i$ to obtain $f_i$. The central idea, is that the behaviour of the function at unknown points may be estimated by some prior assumptions. Additional information from data that has already been observed is then used to derive the conditional probability distribution of the function, given those observations. This set of, $n$, observations is called the `training set', $\mathcal{D}$, has inputs arranged in the matrix $\vec{X}$. In the general form, $\mathcal{D} = \{(\vec{x}_i, f_i) ~|~ i = 1: n \}$. It is assumed that the dataset can be modelled by a gaussian distribution, defined as

 \begin{equation} \label{eq:GPR}
\vec{f} \sim \mathcal{N}(\boldsymbol{\mu}, \vec{K}),
\end{equation}

\noindent that is, every member of the dataset is part of a Gaussian distribution, $\mathcal{N}$, centred at $\boldsymbol{\mu}$ with spread defined by the matrix of similarities, $\vec{K}$. The matrix of similarities is defined by a `kernel' which is interchangeable within the GPR framework and the particular choice of which is based upon prior assumptions made about the particular situation. 

Unknown, `test', data points, $\vec{X}_*$, of which there are $n_*$, must come from the same distribution, so $\vec{f}$ and $\vec{f}_*$ may be described as a joint vector of samples from that multivariate Gaussian,

\begin{equation}
\binom{\vec{f}}{\vec{f}_*} \sim \mathcal{N} \left( \binom{\boldsymbol{\mu}}{\boldsymbol{\mu}_*} , \begin{pmatrix} \vec{K} & \vec{K}_* \\ \vec{K}^T_* & \vec{K}_{**} \end{pmatrix} \right).
\end{equation}

\noindent Here, $\vec{K} = \kappa(\vec{X}, \vec{X})$ is $n \times n$, $\vec{K_*} = \kappa(\vec{X}, \vec{X_*})$ is $n \times n_*$ and $\vec{K}_{**} = \kappa(\vec{X_*}, \vec{X_*})$ is $n_* \times n_*$ where $\kappa$ is a function defining the kernel. In this case, 

\begin{equation}
\kappa (x,x') =  \sigma ^2_f \exp \left(- \frac{1}{2l^2}(x - x')^2 \right),
\end{equation}

\noindent which represents the prior assumption that similar inputs will beget similar outputs, and the similarity of the outputs can be estimated by the exponent of the squared difference of the inputs. Note the `hyper-parameters' $\sigma ^2_f$ and $l$ which enable the fine tuning of this expression depending upon how similar nearby points are expected to be and the confidence of that expectation. 

Thus, from $\vec{X}$ and $\vec{X}_*$ the joint distribution may be derived, and so determining the distribution of $\vec{f}_*$ at the point of interest is simply a case of determining the conditional distribution (as parametrised by the mean, $\mu _*$ and the variance $\Sigma_*$),

\begin{equation}
\begin{aligned}
p(\vec{f_*} | \vec{X_*}, \vec{X}, \vec{f}) &= \mathcal{N}(\vec{f_*} | \boldsymbol{\mu}_* , \boldsymbol{\Sigma}_*) \\
\boldsymbol{\mu}_* &= \boldsymbol{\mu}(\vec{X}_*) + \vec{K}^T_* \vec{K}^{-1}(\vec{f} - \boldsymbol{\mu}(\vec{X})) \\
\boldsymbol{\Sigma}_* &= \vec{K}_{**} - \vec{K}^T_* \vec{K}^{-1} \vec{K}_*.
\end{aligned}
\end{equation}

\noindent Consequently, for any unknown point 

 \begin{equation} \label{eq:GPR}
f_* \sim \mathcal{N}(\mu_*, \Sigma_*),
\end{equation}

\noindent and so the distributions of unknown values of the function may be estimated over a grid. This facilitates the representation of the continuous function through construction of the \textit{posterior} distribution. This is visualised by plotting $\boldsymbol{\mu}_*$ and $\boldsymbol{\Sigma}_*$ which represents an estimate of the function and the uncertainty of the estimate respectively.

The major benefit of this technique is that it captures even highly non linear relationships, and that it reflects high levels of confidence nearby known training dataset points and highlights the increased uncertainty that exists away from these points.


\section{Ensemble Model} \label{sec:ES}

The ensemble process of determining the array size and design is managed by the collocation model, for ease of reference this is called \textit{TidalArraySizer}. This interfaces with \textit{OpenTidalFarm} initially to build up a GPR surrogate. Using this surrogate, \textit{TidalArraySizer} then identifies regions of the function with high potential or where there is high uncertainty and selects a new point to be run by \textit{OpenTidalFarm}.

By looking at the geometry of the turbine area and the spacing constraints on the turbines, it is possible to determine the maximum packing of the turbines and therefore the maximum number of turbines the site can support. Clearly, if the site is fully packed, there is no scope to optimise the micro-siting, so this enables a quick evaluation of the power output of the most populous array possible for the site, which along with the $N = 0, ~P=0$ data point initialises $\mathcal{D}$, the training set for the GPR. \textit{TidalArraySizer} then samples the point at which there is the greatest uncertainty in the estimated function.

Once $\mathcal{D}$ has datapoints, it is clear that the optimum will be located in one of the two intervals between those points (or, by chance, at one of those data points). The next sample point is taken in the interval with the highest potential (i.e. where the mean function reaches its maximum) then once again at the point with the greatest uncertainty. This point is added to $\mathcal{D}$, the GPR is recalculated and the next iteration begins. This process is shown in \ref{fig:ensembleflowchart}.

It is important to remember that the micro-siting optimisation comes at a computational expense nearly independent of the number of turbines, as such, there is no detriment to sampling the highly populated arrays as compared to smaller arrays. The computational expense can be controlled, however, by settings such as mesh coarseness and \textit{OpenTidalFarm'}s convergence criteria, as \textit{TidalArraySizer} homes in on the optimum $N$ these settings are tightened up, so as to obtain more accurate evaluations around the function maximum.

%The array design problem is tackled through the following steps, shown in figure \ref{fig:ensembleflowchart}. The search space is initialised by the designer who selects the probable range of values into which the optimum array size will fall. This range is then orthogonally sampled and $P$ is determined for $N$ at each sample point by running \textit{OpenTidalFarm} for each value of $N$. This establishes a database of functional evaluations, used as the initial training set, $\mathcal{D}_0$. GPR is used on $\mathcal{D}_0$, and the posterior distribution of the relaxed function, $P^R(N)$ is found. Areas of high uncertainty coupled with high potential are identified and promising points are ranked for investigation. For the $i$th iteration, the most promising point is evaluated by running \textit{OpenTidalFarm} for that number of turbines, and the result is added to the training set, now $\mathcal{D}_i$. The GPR model constructs a new posterior distribution based on $\mathcal{D}_i$ and new potential areas are investigated. The model terminates when the functional maximum has been found to within a certain tolerance.

 % % % F L O W   C H A R T % % %

\input{flow_chart}

\section{Model Calibration}

Calibration of the model is important in order to determine the appropriate kernel function and the associated hyper-parameters and in order to quantify how \textit{TidalArraySizer} tweaking parameters on an \textit{OpenTidalFarm} run may alter the solution returned and the speed of the solve. And furthermore, how the noise created by the simplification may be quantified and modelled.

In order to do this a large number of different set ups were run for a 


\section{Example one, optimising to maximise power output} \label{sec:EX1}

The model was run on a two-dimensional domain with steady, uni-directional flow. The domain, $\Omega$ was 1 km by 2.2 km $(x,y)$ on plan with inflow in the $y$ direction across $x=0$, outflow across $x = 1 $ km and solid, frictionless boundaries at $y = 0$ and $y = 2.2$ km.  The turbine domain (the area in which turbines can be located) was rectangular in shape, 0.3 km by 0.6 km, centered in the domain in the $x$ direction with and spanning from $y = 1.1$ km to $y = 1.7$ km. Given inter-turbine spacing constraints of 20 m in both $x$ and $y$ diections, the maximum number of turbines that could be specified on the site was 160, and the ensemble model was set to sample this whole range for the training dataset. Samples were taken $N = 10i ~~\forall~ \{ i \in \mathbb{Z} ~|~ 1 \geq i \geq 15 \}$. $GP(N)_0$ is shown in figure XXX in which the training set is shown as points, the posterior distribution is shown by the line, which records the mean, and the shading which denotes the $\pm$ 95\% confidence interval.

\begin{figure}[h!]
\centering
%\includegraphics[width = 1\textwidth, keepaspectratio=true]{media/example1.pdf}
\caption{Graph of power output from varying array size showing initial dataset of observed values, estimated values and 95\% confidence interval.}
\label{fig:simplechannel01}
\end{figure}

%\begin{figure}[h]
%\begin{center}$
%\begin{array}{cc}
%\includegraphics[width=0.5\textwidth]{media/simple_channel_02.png}&
%\includegraphics[width=0.5\textwidth]{media/simple_channel_03.png}\\
%\includegraphics[width=0.5\textwidth]{media/simple_channel_04.png}&
%\includegraphics[width=0.5\textwidth]{media/simple_channel_turbs.png}
%\end{array}$
%\end{center}
%\caption{High uncertainty and high potential $N$ are tested until convergence criteria are met, final layout %shown in (d).}
%\label{fig:simplechannel02}
%\end{figure}

As expected, adding more turbines to the site increases the power extracted from it, and in fact the turbine site is extracts the most energy once fully populated. As such, the termination criteria of the algorithm have been met having analysed the initial training set. However the increase in power per additional turbine has decreased -- almost logarithmically -- up until this point. With no cost attributed to the addition of turbines, the decreasing efficiency of the farm (as measured by power per turbine) is not taken into account. Consequently, a financially based approach is proposed to penalise decreasing efficiency from overly large farms.


\section{Adding a Cost Model} \label{sec:CM}

As illustrated above, in general, optimisation of the number of turbines to maximise the power output of an array does not provide the industrial designer with a practically viable design of the array. Additionally, resource estimation which omits a cost model will likely be over-predict the feasibly exploitable resource. 

The financial costs associated with construction of a tidal current turbine array may be categorised as those which depend upon the number of turbines and those which depend upon the location of those turbines -- for example turbines located in deeper water will be more costly to install.

Costs which are a related to the array size are, by far, the most significant and so will be the primary focus for this work. Thus \eqref{eq:int_main} becomes a maximisation of investment return

\begin{equation} \label{eq:int_revised}
\underset{N}{\text{max}} \hspace{0.5cm} I(P(N)) - C(N),
\end{equation}

\noindent where $I: \mathbb{R} \to \mathbb{R}$ is a function converting the power output into a measure of financial income and $C$ does the same with costs. 

Unfortunately the process of converting from physical units into financial ones is highly subjective and dependent on a great number of factors -- many of which are proprietorial to turbine manufacturers (e.g. the sales cost model for turbines). The code has been designed in a modular fashion such that alternate cost models may be implemented by simply replacing the default. For the purposes of demonstration, for this paper certain assumptions have been made, upon which $I(P)$ and $C(N)$ have been designed.

\citet{allan2011levelised} calculate the costs and income from a hypothetical 100 MW installed capacity of tidal stream power production plant. Based upon the assumption that the average hourly delivery to the grid is 33 \% of the rated capacity, and the plant lifetime is 20 years, the lifetime energy delivered to the grid is 5.78 TWh. Based upon their cost research and assumptions, a levelised cost of energy of \pounds81.25 MWh$^{-1}$ is determined. For this work, it shall be assumed that the present value of the lifetime income of the array will simply scale linearly with installed capacity, i.e. each mega Watt of installed capacity is worth just under \pounds470m in net present value. It is further assumed that the pre-development, construction and operation \& maintenance costs will also scale linearly with the number of turbines, the present value costs of these are \pounds1,528,139, \pounds97,234,188 and \pounds14,112,965 respectively \citep{allan2011levelised}. Assuming 100 MW of capacity is provided by 1.2 MW turbines, this represents 84 turbines, at a total lifetime present value cost of \pounds 112.9 m.

These crude assumptions yield an income of \pounds 4.7 m MW$^{-1}$ of installed capacity, at a cost of \pounds1.34 m turbine$^{-1}$

\begin{equation} \label{eq:int_revised}
\begin{aligned}
I(P) &= 4.7 \times 10^6 P \\
C(N) &= 1.34 \times 10^6 N.
\end{aligned}
\end{equation}

\noindent This revised model  yields the results in sections \ref{sec:EX1.2} and \ref{sec:EX2.1}.

\section{Example one, optimising to maximise return on investment} \label{sec:EX1.2}

Re-examining the simple channel example, this time optimising the size of the array for the return on investment...

I haven't run it yet but it's basically what is in my google doc...


%Look, the maximum is hard to discern.

%\subsection{Discussion}

%Diminishing return on investment




%Clearly the \textit{number} of turbines being purchased and installed has the largest single impact on the cost of an array. However, just as manipulating the turbine micro-siting to maximise power output may impact upon the viability of the project, so too may certain \textit{costs} be dependent on turbine location and have just as significant an impact on viability.





% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%For most problems, the variogram is unknown, so various parameters must be assumed and these can significantly affect the quality of the predictions made by the GPR model. However, for this work, a variogram has been constructed through analysis of three varied, idealised models, using

%\begin{equation} \label{eq:variogram}
%2\gamma (h) = \frac{1}{2n} \sum_i^n (P(N_i) - P(N_{i+h}))^2
%\end{equation} %\hfill <insert citation>

%The model is initialised with a database of values at known points, through this database and the variogram the kriged value at each point is constructed from

%\begin{equation} \label{eq:kriging}
%\text{Var}(y^* - y_0) = - \sum_\alpha \sum_\beta \lambda_\alpha \lambda_\beta \gamma_{\alpha \beta} +2 \sum_\alpha \gamma_{\alpha 0}
%\end{equation} %\hfill <insert citation>

%\noindent where I know neither the values represented by the variables, nor the meaning of the quantity being calculated.

%\vspace{10mm}
%ALTERNATE EXPLANATION LIFTED FROM SOMEWHERE ELSE
%\vspace{10mm}

%A non-parametric regression model for creating a function which maps $N$ to $P$ based upon a limited set of training data. For unknown values of $N$, $P$ is calculated point-wise as a weighted average of nearby known values, the expected error of these estimates can also be returned. This is a key advantage is areas of both high potential and high uncertainty can be identified and investigated.

%The Kriging model is typically expressed as:

%\begin{equation} 
%y(X) = \beta + Z(\vec{x})
%\end{equation}

%where $\beta$ represents a constant term of the model, and $Z(x)$ is a zero mean Gaussian stochastic process. The covariance matrix of $Z(x)$ is given by 

%\begin{equation}
%\text{cov}(Z(\vec{x}^i),Z(\vec{x}^j)) = \sigma^2R(\vec{x}^i,\vec{x}^j)
%\end{equation}

%where $\sigma^2$ is the so called process variance and $R(\cdots)$ is the correlation function. Different types of correlation functions can be employed. A commonly used type of correlation function is the Gaussian kernel 

%\begin{equation}
%R(\vec{x}^i,\vec{x}^j) = \prod_{k=1}^n exp(-\theta_k | x_k^i - x_k^j | ^{p_k}) 
%\end{equation}

%where $\theta_k \geq 0$ and $0 < p_k \leq 2$ are the hyperparameters. Note that the above equation asserts that there is a complete correlation of a point with itself and this correlation deteriorates rapidly as the two points move away from each other in the parameter space. The choice of pk = 2 would provide enough flexibility for modelling smooth and highly non-linear functions for most cases.
 % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %









%Price of electricity assumptions

%Cost per turbine and per unit electricity assumptions

%While the intricacies of the energy market and the complexity of large-scale infrastructure investments such as for power plants are, by no means captured by the proposed cost model, it does represent a robust characterisation of the problem for the purposes of scoping an array at the early specification stages, and for the purposes of determining the likely magnitude of the tidal resource that can practically be accessed.

%Plotting R(N) yields the following results for the case studies introduced in section XXX:


\section{Example two, optimising to maximise return on investment} \label{sec:EX2.1}

Optimise the number of turbines in ISPF based upon return.

\section{Conclusions and future work}

%\section{Alternate Functionals}

%$I(c)$ may be improved if all turbines are expected to work with a range of power generation - i.e. no one turbine is working excessively more or less than any other.

%Different ways to measure $C(m)$ - 


%\section{Generic Thoughts}

%Starting point - determine aggregate flow direction across turbine area, find max row length that will fit in the that direction within site \& seed with that number of turbines.

%Determine whether row width of $x$ turbines is linked to our peak value of power output - make sites longer in line with flow - to allow room for lots of crap turbines and see if the power generated begins to decrease.....

%As noted by \citet{zhou2004hierarchical} a good `outer' or \textit{global} surrogate model should accurately represent the problem landscape and be computationally cheap to compute. For this work, a Gaussian process regression, GPR, approach has been selected to model the integer problem relaxed onto a continuous space. %$P^R \in \mathcal{C}^1(R), ~~~ P^R(N) = P(N) ~~ \forall N \in \mathbb{N}$

%GPR is a non-parametric regression model for creating an input-output mapping from a training set of empirical data \citep{rasmussen2006gaussian}. In this case, the training dataset, $\mathcal{D}$, of $n$ observations takes the form $\mathcal{D} = \{(N_i, P_i) ~|~ i = 1, \dots, n \}$. In its most general form,

%\begin{equation} \label{eq:GPR}
%P^R(N) \sim \mathcal{GP}(N)
%\end{equation} %\hfill <insert citation>

%\noindent where $P^R(N)$ is $P(N)$ relaxed onto a continuous space and $\mathcal{GP}$ is the GPR of $N$ parametrised through a covariance function, $k$.

%Consider a one-dimensional spatially varying function $y(x)$. The key assumption of the GPR model in a frequentist framework, is that error $\epsilon$ between $y(x_i)$ and $y(x_i+h)$ will be related to the proximity of the two points, $h$. That is to say that two samples taken close together will probably be more similar than two samples taken further from each other. The correlation between two points is calculated from $k$, which is effectively a measure of the spatial continuity of the function. Thus, unknown, `test', values may be estimated from combining the nearby known values from the training set, weighted by their proximity, via $k$.

%Many variations of the basic model exist and the interested reader is directed towards \cite{sacks1989design} which originally popularised the technique. However, the model is frequently expressed in the generic form

%\begin{equation} %\label{eq:GPR}
%y(\vec{x}) = \beta + Z(\vec{x})
%\end{equation} \hfill \cite{zhou2004hierarchical}

%\noindent where $\vec{x}$ is a (potentially multi-dimension) input, $\beta$ is a model constant and $Z(\vec{x})$ is a zero-mean stochastic process for which the covariance matrix is

%\begin{equation} %\label{eq:GPR}
%Cov(Z(\vec{x}^i),Z(\vec{x}^j)) = \sigma ^2 R(\vec{x}^i, \vec{x}^j).
%\end{equation} \hfill \cite{zhou2004hierarchical}

%\noindent here, $\sigma$ is the variance and $R$ is the correlation function, through which we parametrise the model. Different correlation functions may be used depending on the scenario and it is this flexibility that makes GPR so widely applicable. Of course the drawback of such flexibility is that the quality of the GPR model is dependent on the how well the correlation function represents the process being modelled. 

%The function estimate, $\hat{y}$ at a point, $\vec{x}^*$ is 

%\begin{equation} %\label{eq:GPR}
%\hat{y}(\vec{x}^*) = \hat{\beta} + \vec{r}^T \vec{R}^{-1}(\vec{y} - \vec{1} \hat{\beta})
%\end{equation} \hfill \cite{zhou2004hierarchical}

%\noindent where $\vec{1} = (1, 1, \cdots,1)^T$, $\vec{R}$ is the correlation matrix and $\vec{r}(\vec{x}^*)$ is the correlation vector between $\vec{x}^*$ and the points in $\mathcal{D}$, $\vec{r}(\vec{x}^*) =(R(\vec{x}^*, \vec{x}^1), \dots R(\vec{x}^*, \vec{x}^n))$. The variance of the prediction, $s^2(\vec{x}^*)$ can also be found using

%\begin{equation} %\label{eq:GPR}
%s^2(\vec{x}^*) = \sigma ^2 [1 - \vec{r} \vec{R} ^{-1} \vec{r} + \frac{(1- \vec{1} ^T \vec{R} ^{-1} \vec{r}) ^2}{\vec{1} ^T \vec{R} ^{-1} \vec{r}}]
%\end{equation} \hfill \cite{zhou2004hierarchical}

%Thus, in the tidal array sizing problem this method may be employed so that, for test values of $N$, $P$ can be estimated from $\mathcal{D}$. The expected error of these estimates is also returned, which is a key advantage of the approach, as areas of both high potential and high uncertainty can be identified and investigated. 



%\section{Acknowledgements}

%Acknowledgements and that.

\section{Bibliography}

\bibliographystyle{elsarticle-harv}
\bibliography{bibio.bib}

\end{document}
